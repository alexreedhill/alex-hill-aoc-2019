(ns alex-hill-aoc-2019.shared.intcode-test
  (:require [clojure.test :refer :all]
            [alex-hill-aoc-2019.shared.intcode :as intcode]))

(deftest intcode
  (testing "addition"
    (is (= (intcode/run [1 0 0 0 99]) [2 0 0 0 99])))

  (testing "multiplication"
    (is (= (intcode/run [2 3 0 3 99]) [2 3 0 6 99])))

  (testing "more multiplication"
    (is (= (intcode/run [2 4 4 5 99 0]) [2 4 4 5 99 9801])))

  (testing "something complicated"
    (is (= (intcode/run [1 1 1 4 99 5 6 0 99]) [30 1 1 4 2 5 6 0 99])))

  (testing "handle parameter modes"
    (is (= (intcode/run [1002 4 3 4 33]) [1002 4 3 4 99])))

  (testing "input"
    (is (= (intcode/run [3 0 99] 1337) [1337 0 99])))

  (testing "negative numbers"
    (is (= (intcode/run [1101 -2 2 0 99]) [0 -2 2 0 99]))))


