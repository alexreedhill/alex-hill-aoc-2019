# Advent Of Code 2019 - Clojure

This repository is a short demonstration of my clojure skills thus far. The meatiest logic lives in shared/intcode.clj. It defines a virtual "intcode computer" was that was built iteratively to satisfy the requirements of days 2 and 5.

New to advent of code? You can find it [here](https://adventofcode.com/2019).
