(ns alex-hill-aoc-2019.day_2.core
  (:require [alex-hill-aoc-2019.shared.intcode :as intcode]
            [alex-hill-aoc-2019.shared.util :as util]))

(def puzzle-input
  (->
    "src/alex_hill_aoc_2019/day_2/input.txt"
    slurp
    clojure.string/trim))

(defn generate-case [noun verb]
  (->
    (map util/parse-int (clojure.string/split puzzle-input #","))
    (vec)
    (assoc 1 noun)
    (assoc 2 verb)))

(dotimes [noun 99]
  (dotimes [verb 99]
    (let [case-input (generate-case noun verb)
          result (intcode/run case-input)]
      (if (= (first result) 19690720)
        '(noun verb)))))
