(ns alex-hill-aoc-2019.day_1.core
  (:require [alex-hill-aoc-2019.shared.util :as util]))

(def input-file
  "src/alex_hill_aoc_2019/day_1/input.txt")

(defn fuel-needed-for-mass [mass]
  (-> mass
    (/ 3)
    (Math/floor)
    (- 2)
    (max 0)))

(defn fuel-needed-for-module [mass]
  (loop [my-mass mass
         total-fuel 0]
    (let [my-fuel (fuel-needed-for-mass my-mass)]
      (if (= my-fuel 0)
        total-fuel
        (recur my-fuel (+ my-fuel total-fuel))))))

(->>
  (slurp input-file)
  (clojure.string/split-lines)
  (map util/parse-int)
  (map fuel-needed-for-module)
  (reduce +))


