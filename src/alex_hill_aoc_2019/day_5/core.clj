(ns alex-hill-aoc-2019.day-5.core
  (:require [alex-hill-aoc-2019.shared.intcode :as intcode]
            [alex-hill-aoc-2019.shared.util :as util]))

(defn run-with-input [input]
  (->
    "src/alex_hill_aoc_2019/day_5/input.txt"
    slurp
    clojure.string/trim
    (clojure.string/split #",")
    ((fn [input] (map util/parse-int input)))
    (vec)
    (intcode/run input)))

; Part 1
(run-with-input 1)

; Part 2
(run-with-input 5)
