(ns alex-hill-aoc-2019.day-4.core
  (:require [alex-hill-aoc-2019.shared.util :as util]))

(def range-min 183564)
(def range-max 657474)

(defn valid? [n]
  (let [digits (util/num-to-digits n)
        matches (partition-by identity digits)]
    (and
      (some #(= (count %1) 2) matches)
      (apply <= digits))))

(loop [current range-min
       count 0]
  (if (= current range-max)
    count
    (if (valid? current)
      (recur (+ 1 current) (+ 1 count))
      (recur (+ 1 current) count))))

