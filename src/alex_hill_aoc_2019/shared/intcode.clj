(ns alex-hill-aoc-2019.shared.intcode
  (:require [alex-hill-aoc-2019.shared.util :as util]))

(defn pad-opcode [opcode]
  (if (< (count opcode) 4)
    (pad-opcode (conj opcode 0))
    opcode))

(defn parse-opcode [program instruction-pointer]
  (->
    instruction-pointer
    program
    util/num-to-digits
    pad-opcode))

(def parameter-modes
  {
   0 (fn [program param] (nth program param))
   1 (fn [_ param] param)
   })

(defn values-in-mode [program param-1 param-2 param-1-mode param-2-mode]
  [
   ((parameter-modes param-1-mode) @program param-1)
   ((parameter-modes param-2-mode) @program param-2)
   ])

(defn execute-combine-instruction [program instruction-pointer operator]
  (let [[param-2-mode param-1-mode _ _] (parse-opcode @program @instruction-pointer)
        end (+ @instruction-pointer 4)
        instruction (subvec @program @instruction-pointer end)
        [_ param-1 param-2 output-address] instruction
        [param-1-value param-2-value] (values-in-mode program param-1 param-2 param-1-mode param-2-mode)]
    (do
      (swap! program assoc output-address (operator param-1-value param-2-value))
      (reset! instruction-pointer end))))

(defmulti execute-instruction
  (fn [program instruction-pointer _]
    (last (parse-opcode @program @instruction-pointer))))

(defmethod execute-instruction 1 [program instruction-pointer _]
  (execute-combine-instruction program instruction-pointer +))

(defmethod execute-instruction 2 [program instruction-pointer _]
  (execute-combine-instruction program instruction-pointer *))

(defmethod execute-instruction 3 [program instruction-pointer input]
  (let [end (+ @instruction-pointer 2)
        instruction (subvec @program @instruction-pointer end)
        [_ output-address _ _] instruction]
    (do
      (swap! program assoc output-address input)
      (reset! instruction-pointer end))))

(defmethod execute-instruction 4 [program instruction-pointer _]
  (let [end (+ @instruction-pointer 2)
        instruction (subvec @program @instruction-pointer end)
        [_ output-address _ _] instruction]
    (do
      (println (@program output-address))
      (reset! instruction-pointer end))))

(defn execute-boolean-instruction [program instruction-pointer operator]
  (let [[param-2-mode param-1-mode _ _] (parse-opcode @program @instruction-pointer)
        end (+ @instruction-pointer 3)
        instruction (subvec @program @instruction-pointer end)
        [_ param-1 param-2] instruction
        [bool-value new-address] (values-in-mode program param-1 param-2 param-1-mode param-2-mode)]
    (if (operator (= 0 bool-value))
      (reset! instruction-pointer new-address)
      (reset! instruction-pointer end))))

(defmethod execute-instruction 5 [program instruction-pointer _]
  (execute-boolean-instruction program instruction-pointer not))

(defmethod execute-instruction 6 [program instruction-pointer _]
  (execute-boolean-instruction program instruction-pointer identity))

(defn execute-compare-instruction [program instruction-pointer operator]
  (let [[param-2-mode param-1-mode _ _] (parse-opcode @program @instruction-pointer)
        end (+ @instruction-pointer 4)
        [_ param-1 param-2 output-address] (subvec @program @instruction-pointer end)
        [param-1-value param-2-value] (values-in-mode program param-1 param-2 param-1-mode param-2-mode)
        result (if (operator param-1-value param-2-value) 1 0)]
    (swap! program assoc output-address result)
    (reset! instruction-pointer end)))

(defmethod execute-instruction 7 [program instruction-pointer _]
  (execute-compare-instruction program instruction-pointer <))

(defmethod execute-instruction 8 [program instruction-pointer _]
  (execute-compare-instruction program instruction-pointer =))

(defn run
  ([program]
   (run program nil))
  ([program input]
   (let [program-state (atom program)
         instruction-pointer (atom 0)]
     (while (not (= 99 (@program-state @instruction-pointer)))
       (execute-instruction program-state instruction-pointer input))
     @program-state)))
