(ns alex-hill-aoc-2019.shared.util)

(defn parse-int [string]
  (Integer/parseInt string))

(defn num-to-digits [n]
  (map #(Character/digit % 10) (str n)))
